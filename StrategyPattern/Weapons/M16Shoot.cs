﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern.Weapons
{
    class M16Shoot : IShoot
    {
        public void Shoot()
        {
            Console.WriteLine("Shoot from M16");
        }
    }
}
