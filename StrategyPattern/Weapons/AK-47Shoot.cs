﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern.Weapons
{
    class AK_47Shoot : IShoot
    {
        public void Shoot()
        {
            Console.WriteLine("Shoot from AK-47");
        }
    }
}
