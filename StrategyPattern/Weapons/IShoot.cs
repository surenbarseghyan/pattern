﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern.Weapons
{
   public interface IShoot
    { 
        void Shoot();
    }
}
