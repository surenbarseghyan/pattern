﻿using StrategyPattern.Soldiers;
using StrategyPattern.Weapons;
using System;

namespace StrategyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            AmericanSoldier american = new AmericanSoldier();
            american.BeginSpeak();
            american.BeginShoot();
            Console.WriteLine();
            RussianSoldier russian = new RussianSoldier();
            russian.BeginSpeak();
            russian.BeginShoot();
            russian.SetWeapon(new M16Shoot());
            russian.BeginShoot();
            
        }
    }
}
