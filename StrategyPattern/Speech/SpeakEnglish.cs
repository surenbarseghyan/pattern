﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern.Speech
{
    class SpeakEnglish : ISpeak
    {
        public void Speak()
        {
            Console.WriteLine("I'm speaking English");
        }
    }
}
