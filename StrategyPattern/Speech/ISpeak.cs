﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern.Speech
{
   public interface ISpeak
    {
        void Speak();
    }
}
