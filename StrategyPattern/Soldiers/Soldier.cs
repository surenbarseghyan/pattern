﻿using StrategyPattern.Speech;
using StrategyPattern.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern.Soldiers
{
    public abstract class Soldier
    {
        public string Name { get; set; }
        protected ISpeak speakBehavior;
        protected IShoot shootBehavior;
        public void BeginSpeak()
        {
            speakBehavior.Speak();
        }
        public void BeginShoot()
        {
            shootBehavior.Shoot();
        }
        public void SetLanguage(ISpeak speak)
        {
            speakBehavior = speak;
        }
        public void SetWeapon(IShoot shoot)
        {
            shootBehavior = shoot;
        }
        public void Walk()
        {

        }
        public void Run()
        {

        }
    }
}
