﻿using StrategyPattern;
using StrategyPattern.Speech;
using StrategyPattern.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern.Soldiers
{
    class RussianSoldier:Soldier
    {
        public RussianSoldier()
        {
            speakBehavior = new SpeakRussian();
            shootBehavior = new AK_47Shoot();
        }
    }
}
