﻿using StrategyPattern.Speech;
using StrategyPattern.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern.Soldiers
{
    class AmericanSoldier:Soldier
    {
        public AmericanSoldier()
        {
            speakBehavior = new SpeakEnglish();
            shootBehavior = new M16Shoot();
        }
    }
}
